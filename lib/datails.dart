import 'package:flutter/material.dart';

import 'model/models.dart';

class DetailsPage extends StatefulWidget {
  Plant plant;
  DetailsPage(this.plant);
  @override
  _DetailsPageState createState() => _DetailsPageState(this.plant);
}

class _DetailsPageState extends State<DetailsPage> {
  Plant plant;
  _DetailsPageState(this.plant);

  @override
  Widget build(BuildContext context) {
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            /*1*/
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                /*2*/
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text(
                    plant.name,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
                Text(
                  plant.alt_name,
                  style: TextStyle(
                    color: Colors.grey[500],
                  ),
                ),
              ],
            ),
          ),
          /*3*/
          Icon(
            Icons.remove_red_eye,
            color: Colors.blueAccent,
          ),
          Text('41'),
        ],
      ),
    );

    Widget textSection = Container(
      padding: const EdgeInsets.all(32),
      child: Text(
        plant.description,
        softWrap: true,
      ),
    );

    return Scaffold(
      appBar: AppBar(
        title: Text(plant.name),
      ),
      body: ListView(
        children: [
          Image.network(plant.image_url,
              width: 600, height: 240, fit: BoxFit.cover),
          titleSection,
          textSection,
        ],
      ),
    );
  }
}
