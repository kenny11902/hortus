import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:hortus/model/models.dart';
import './bloc.dart';
import './../shared/data_handler.dart';

class PlantsListBloc extends Bloc<PlantsListEvent, PlantsListState> {
  DataHandler dataHandler = DataHandler();
  @override
  PlantsListState get initialState => PlantsListEmpty();
  @override
  Stream<PlantsListState> mapEventToState(PlantsListEvent event) async* {
    if (event is FetchPlants) {
      yield* _fetchPlantlist(event);
    }
  }

  Stream<PlantsListState> _fetchPlantlist(PlantsListEvent event) async* {
    yield PlantsListLoading();
    try {
      final List<Plant> plantList = await dataHandler.get_all_plants();
      print('list' + plantList.toString());
      yield PlantsLoaded(plants: plantList);
    } catch (_) {
      print(_);
      yield PlantsListError();
    }
  }
}
