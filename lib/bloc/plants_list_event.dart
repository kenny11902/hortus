import "package:equatable/equatable.dart";
import 'package:flutter/material.dart';
import 'package:hortus/model/models.dart';

abstract class PlantsListEvent extends Equatable {
  const PlantsListEvent();
  @override
  List<Object> get props => [];
}

class FetchPlants extends PlantsListEvent {}
