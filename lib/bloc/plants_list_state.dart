import 'package:hortus/model/models.dart';
import 'package:meta/meta.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

abstract class PlantsListState extends Equatable {
  const PlantsListState();

  @override
  List<Object> get props => [];
}

class PlantsListEmpty extends PlantsListState {}

class PlantsListLoading extends PlantsListState {}

class PlantsListError extends PlantsListState {}

class PlantsLoaded extends PlantsListState {
  final List<Plant> plants;

  const PlantsLoaded({@required this.plants}) : assert(plants != null);

  @override
  List<Object> get props => [plants];
}
