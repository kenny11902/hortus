import 'dart:convert';
import 'package:hortus/model/models.dart';
import 'package:http/http.dart' as http;

class Connector {
  String server;
  String ip;

  Future<List<Plant>> get_plants() async {
    List<Plant> plantlist = [];
    Map<String, dynamic> plantData;
    final http.Response response = await http.get(
      'https://hortus-210c6.firebaseio.com/plants.json',
    );
    plantData = json.decode(response.body);

    plantData.forEach((String Id, dynamic pData) {
      plantlist.add(Plant(
          pData['name'], pData['image_url'], pData['description'],
          alt_name: pData['alt_name']));
    });
    return plantlist;
  }
}
