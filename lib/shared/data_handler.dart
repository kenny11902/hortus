import 'package:hortus/model/plant.dart';
import 'package:hortus/shared/connector.dart';

class DataHandler {
  Connector connector = Connector();

  Future<List<Plant>> get_all_plants({int index = 0}) async {
    // wrapper
    return this.connector.get_plants();
  }
}
