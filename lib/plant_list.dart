import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hortus/datails.dart';
import 'package:hortus/bloc/bloc.dart';

import 'model/models.dart';

class PlantList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return BlocBuilder<PlantsListBloc, PlantsListState>(
      builder: (context, state) {
        if (state is PlantsListEmpty) {
          BlocProvider.of<PlantsListBloc>(context).add(FetchPlants());
          return CircularProgressIndicator();
        } else if (state is PlantsListLoading) {
          return CircularProgressIndicator();
        } else if (state is PlantsLoaded) {
          final List<Plant> plants = state.plants;
          return ListView.builder(
            scrollDirection: Axis.vertical,
            shrinkWrap: true,
            itemCount: plants.length,
            itemBuilder: (context, index) {
              final Plant plant = plants[index];
              return Container(
                color: (index % 2 == 0) ? Colors.greenAccent : Colors.green,
                child: ListTile(
                  leading: ClipOval(
                    child: FadeInImage.assetNetwork(
                      placeholder: 'assets/plant.jpg',
                      image: plant.image_url,
                      fit: BoxFit.cover,
                      height: 50.0,
                      width: 50.0,
                    ),
                  ),
                  title: Text(plant.name),
                  onTap: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (BuildContext context) {
                          return DetailsPage(plant);
                        },
                      ),
                    );
                  },
                ),
              );
            },
          );
        } else {
          print(state);
          return Container(
            child: RaisedButton(
              child: Text('pres me'),
              onPressed: () {
                BlocProvider.of<PlantsListBloc>(context).add(FetchPlants());
              },
            ),
          );
        }
      },
    );
  }
}
