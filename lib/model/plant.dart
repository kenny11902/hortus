class Plant {
  String name;
  String alt_name;
  String image_url;
  String description;

  Plant(this.name, this.image_url, this.description, {this.alt_name = ''});
  static Plant fromJson(dynamic json) {
    final plantData = json[0];
    return Plant(
      plantData.name,
      plantData.image_url,
      plantData.description,
      alt_name: plantData.alt_name,
    );
  }
}
